<?php

namespace App\Repository;
use App\Entity\Language;
use App\Entity\Message;
use App\Entity\Student;
use App\Entity\Teacher;
use PDO;
use DateTime;

class TeacherRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de User
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Teacher l'instance de profil
     */

     public function sqlToTeacher(array $line):Teacher{
        return new Teacher($line["name"], $line["lastName"], $line['id_user'], $line['id']);
    }

    /**
     * Renvoit tous les profs de la table teacher
     * @return array renvoit un tableau de teachers de la classe Teacher
     */
    public function findAll(): array
    {
        $teachers = [];
        $statement = $this->connection->prepare('SELECT * FROM teacher');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $teachers[] = $this->sqlToTeacher($item);
        }
        return $teachers;
    }

    /**
     * Permet de trouver un teacher grâce à son id
     * @param int $id l'id du teacher que l'on veut trouver
     * @return Teacher|null Renvoit un teacher
     */
    public function findById(int $id):?Teacher {
        $statement = $this->connection->prepare('SELECT * FROM teacher WHERE id=:id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            return $this->sqlToTeacher($result);
        }
        return null;
    }

    /**
     * Permet de faire persister un nouveau Teacher dans la base de données
     * @param Teacher $teacher le nouveau prof que l'on veut ajouter à la base de données
     * @return void
     */
    public function persist(Teacher $teacher) {
        $statement = $this->connection->prepare('INSERT INTO teacher (id,name,lastName,id_user) VALUES (:id, :name, :lastName, :id_user)');

        $statement->bindValue('name', $teacher->getName());
        $statement->bindValue('lastName', $teacher->getlastName());
        $statement->bindValue('id_user', $teacher->getidUser());
        $statement->bindValue('id', $teacher->getId());
        $statement->execute();
    }

    /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de User avec le bon id
     * @param Teacher $user le nouveau user que l'on veut
     * @return void
     */
    public function update(Teacher $teacher):void {

        $statement = $this->connection->prepare('UPDATE teacher SET name = :name, lastName = :lastName WHERE id = :id');

        $statement->bindValue('name', $teacher->getName(), PDO::PARAM_STR);

        $statement->bindValue('lastName', $teacher->getlastName(), PDO::PARAM_STR);

        $statement->bindValue('id', $teacher->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un user de la base de donnée à partir de son id
     * @param int $id, l'id du user à supprimer
     * @return void
     */
    public function delete(Teacher $teacher)
    { 
        $statement = $this->connection->prepare("DELETE FROM user WHERE id =:id");
        $statement->bindValue("id", $teacher->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

/**
     * Renvoit la liste de profs contactés
     * @return array renvoit un tableau de teachers de la classe Teacher
     */
    public function findByMessage(int $idStudent): array
    {
        $teachers = [];
        $statement = $this->connection->prepare('SELECT DISTINCT teacher.* 
        FROM teacher 
        INNER JOIN message 
        ON teacher.id = message.id_teacher
        WHERE message.id_student = :id');
        $statement->bindValue(":id", $idStudent, PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $teachers[] = $this->sqlToTeacher($item);
        }
        return $teachers;
    }

    /**
     * Permet de trouver les messages envoyés aux profs selon leur id
     * @param Message $message les message correspondant aux profs contactés
     * @return array renvoit un tableau de messages
         */ 
    public function findMessageByTeacher(int $id):array
    { 
        $array = [];
        $statement = $this->connection->prepare("SELECT DISTINCT message.*
        FROM teacher 
        INNER JOIN message 
        ON teacher.id = message.id_teacher
        WHERE message.id_teacher = :id
        ");

        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        
        $statement->execute();
        
        $results = $statement->fetchAll();
            foreach($results as $line){
                $messages = new Message($line["content"], $line['author'],$line['id_teacher'], $line['id_student'],new DateTime($line["date"]), $line['id']);
                $array[] = $messages;
            }
        return $array;
    }

    /**
     * Permet de trouver la langue enseignée par un prof
     * @param Language $language les profs dont on veut trouver la langue
     * @return array renvoit un tableau de profs
         */    
        public function findLanguageByTeacher(int $id):?array
        {
            $array = [];
            $statement = $this->connection->prepare('SELECT language.*
            FROM teacher
            JOIN teacher_language ON teacher.id = teacher_language.id_teacher
            JOIN language ON teacher_language.id_language = language.id
            WHERE teacher.id = :id');
    
            $statement->bindValue('id', $id, PDO::PARAM_INT);
            $statement->execute();
            
            $results = $statement->fetchAll();
            foreach($results as $line){
                $language = new Language($line["name"],  $line['id']);
                $array[] = $language;
            }
            return $array;
        }


    /**
     * Récupère l'image d'un enseignant en fonction de son id
     * @param int $teacherId L'ID de l'enseignant
     * @return string|null L'URL de l'image ou null si non trouvé
     */
    public function findImageByTeacherId($teacherId)
    {
        $statement = $this->connection->prepare('SELECT u.image
        FROM user u
        JOIN teacher t 
        ON u.id = t.id_user
        WHERE t.id = :id');
        $statement->bindValue(':id', $teacherId, PDO::PARAM_INT);
        $statement->execute();

        $result = $statement->fetchColumn();

        return $result ? $result : null;
    }

    /**
 * Récupère les informations détaillées de tous les enseignants avec les langues enseignées et l'image de l'utilisateur associé.
 * @return array Un tableau contenant les informations détaillées de tous les enseignants.
 */
public function findAllTeachersWithDetails(): array
{
    $statement = $this->connection->prepare('SELECT teacher.name AS name, teacher.lastName AS lastName, user.image, language.name AS language
    FROM teacher
    JOIN user 
    ON teacher.id_user = user.id
    JOIN teacher_language 
    ON teacher.id = teacher_language.id_teacher
    JOIN language 
    ON teacher_language.id_language = language.id;');

    $statement->execute();

    $results = $statement->fetchAll(PDO::FETCH_ASSOC);

    return $results;
}


/**
 * Récupère les informations détaillées d'un enseignant avec les langues enseignées et l'image de l'utilisateur associé.
 * @param int $teacherId L'ID de l'enseignant
 * @return array|null Les informations détaillées de l'enseignant ou null si non trouvé
 */
public function fetchTeacherDetailsById($teacherId)
{
    $statement = $this->connection->prepare('SELECT teacher.name AS name, teacher.lastName AS lastName, user.image, language.name AS language
    FROM teacher
    JOIN user 
    ON teacher.id_user = user.id
    JOIN teacher_language 
    ON teacher.id = teacher_language.id_teacher
    JOIN language 
    ON teacher_language.id_language = language.id
    WHERE teacher.id = :id');
    
    $statement->bindValue(':id', $teacherId, PDO::PARAM_INT);
    $statement->execute();
    
    $result = $statement->fetch(PDO::FETCH_ASSOC);
    
    return $result ? $result : null;
}


/**
     * Renvoit les élèves ayant envoyé des messages à un professeur donné
     * @param int $teacherId l'ID du professeur
     * @return array renvoit un tableau d'élèves
     */
    public function findStudentsByTeacher(int $teacherId): array
    {
        $students = [];
        $statement = $this->connection->prepare('SELECT DISTINCT student.id AS student_id, student.name, student.lastName
        FROM student
        JOIN message ON student.id = message.id_student
        WHERE message.id_teacher = :id');
        $statement->bindValue(':id', $teacherId, PDO::PARAM_INT);
        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        
        foreach ($results as $item) {
            $student = new Student($item['name'], $item['lastName'], $item['student_id']);
            $students[] = $student;
        }
        
        return $students;
    }



}
