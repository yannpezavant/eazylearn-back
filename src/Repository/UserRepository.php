<?php

namespace App\Repository;
use App\Entity\User;
use PDO;

class UserRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de User
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return User l'instance de profil
     */

     public function sqlToUser(array $line):User{
        return new User($line["role"], $line["image"], $line["mail"], $line["password"], $line['id']);
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité User
     * @return User[]
     */

     public function findAll():array
     {
         $array = [];
 
         $statement = $this->connection->prepare('SELECT * FROM user');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $user){
             $array[] = $this->sqlToUser($user);
         }
         return $array;
     }


    /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un user spécifique à partir de son id et qui le renvoit comme instance de la classe User.
     * @param int $id L'id du user à récupérer
     * @return User le user récupéré, instance de la classe User
     */
    public function findById(int $id):?User{

        $statement = $this->connection->prepare('SELECT * FROM user WHERE id = :id');

        $statement->bindValue(':id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        if($results) {
            return $this->sqlToUser($results);
        }
        return null;
    }

    /** 
     * Méthode permettant de faire persister une instance de Contact sur la base de données
     * @param User le contact à faire persister
     * @return void Aucun retour, mais une fois persisté, le contact aura un id assigné
     */
    
     public function persist(User $user){

        $statement = $this->connection->prepare('INSERT INTO user (role, image, mail, password) VALUES (:role, :image, :mail, :password)');

        $statement->bindValue('role', $user->getRole(), PDO::PARAM_STR);

        $statement->bindValue('image', $user->getImage(), PDO::PARAM_STR);
        
        $statement->bindValue('mail', $user->getMail(), PDO::PARAM_STR);

        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);

        $statement->execute();

        $user->setId($this->connection->lastInsertId());
    }

     /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de User avec le bon id
     * @param User $user le nouveau user que l'on veut
     * @return void
     */
    public function update(User $user):void {

        $statement = $this->connection->prepare('UPDATE user SET role = :role, image = :image, mail = :mail, password = :password WHERE id = :id');

        $statement->bindValue('role', $user->getRole(), PDO::PARAM_STR);

        $statement->bindValue('image', $user->getImage(), PDO::PARAM_STR);
        
        $statement->bindValue('mail', $user->getMail(), PDO::PARAM_STR);

        $statement->bindValue('password', $user->getPassword(), PDO::PARAM_STR);

        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un user de la base de donnée à partir de son id
     * @param int $id, l'id du user à supprimer
     * @return void
     */
    public function delete(User $user)
    { 
        $statement = $this->connection->prepare("DELETE FROM user  WHERE id =:id");
        $statement->bindValue("id", $user->getId(), PDO::PARAM_INT);
        $statement->execute();
        
    }

    public function findByEmail(string $mail):?User {
        
        $connection = Database::connect();
        $query = $connection->prepare('SELECT * FROM user WHERE mail=:mail');
        $query->bindValue(':mail', $mail);
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            return new User($line['role'], $line['image'], $line['mail'], $line['password'], $line['id']);
        }
        return null;
   }

}

