<?php

namespace App\Repository;
use App\Entity\Ranking;
use PDO;

class RankingRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Ranking
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Ranking l'instance de profil
     */

     public function sqlToRanking(array $line):Ranking{
        return new Ranking($line["note"], $line["id_teacher"], $line["id_student"], $line['id']);
    }

/**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité User
     * @return Ranking[]
     */

     public function findAll():array
     {
         $array = [];
 
         $statement = $this->connection->prepare('SELECT * FROM ranking');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $item){
             $array[] = $this->sqlToRanking($item);
         }
         return $array;
     }

     /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un user spécifique à partir de son id et qui le renvoit comme instance de la classe User.
     * @param int $id L'id du user à récupérer
     * @return Ranking le user récupéré, instance de la classe User
     */
    public function findById(int $id):?Ranking {

        $statement = $this->connection->prepare('SELECT * FROM ranking WHERE id = :id');

        $statement->bindValue(':id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        if($results) {
            return $this->sqlToRanking($results);
        }
        return null;
    }

    /** 
     * Méthode permettant de faire persister une instance de Contact sur la base de données
     * @param Ranking le contact à faire persister
     * @return void Aucun retour, mais une fois persisté, le contact aura un id assigné
     */
    
     public function persist(Ranking $ranking){

        $statement = $this->connection->prepare('INSERT INTO ranking (note, id_teacher, id_student) VALUES (:note, :id_teacher, :id_student)');

        $statement->bindValue('note', $ranking->getNote(), PDO::PARAM_STR);

        $statement->bindValue('id_teacher', $ranking->getIdTeacher(), PDO::PARAM_STR);

        $statement->bindValue('id_student', $ranking->getIdStudent(), PDO::PARAM_STR);

        $statement->execute();

        $ranking->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de modifier un ranking dans la base de donnée en lui fournissant une nouvelle instance de ranking avec le bon id
     * @param Ranking $ranking le nouveau ranking que l'on veut
     * @return void
     */
    public function update(Ranking $ranking):void {

        $statement = $this->connection->prepare('UPDATE ranking SET note = :note WHERE id = :id');

        $statement->bindValue('note', $ranking->getNote(), PDO::PARAM_STR);

        $statement->bindValue('id', $ranking->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un user de la base de donnée à partir de son id
     * @param int $id, l'id du user à supprimer
     * @return void
     */
    public function delete(Ranking $ranking)
    { 
        $statement = $this->connection->prepare("DELETE FROM ranking  WHERE id =:id");
        $statement->bindValue("id", $ranking->getId(), PDO::PARAM_INT);
        $statement->execute();
        
    }





}