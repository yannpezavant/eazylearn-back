<?php

namespace App\Repository;
use App\Entity\Language;
use App\Entity\Teacher;
use PDO;

class LanguageRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Language
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Language l'instance de profil
     */

     public function sqlToLanguage(array $line):Language{
        return new Language($line["name"], $line['id']);
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Language
     * @return Language[]
     */

     public function findAll():array
     {
         $array = [];
 
         $statement = $this->connection->prepare('SELECT * FROM language');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $item){
             $array[] = $this->sqlToLanguage($item);
         }
         return $array;
     }

     /**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un Language spécifique à partir de son id et qui le renvoit comme instance de la classe Language.
     * @param int $id L'id du Language à récupérer
     * @return Language le Language récupéré, instance de la classe Language
     */
    public function findById(int $id):?Language{

        $statement = $this->connection->prepare('SELECT * FROM language WHERE id = :id');

        $statement->bindValue(':id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        if($results) {
            return $this->sqlToLanguage($results);
        }
        return null;
    }

    /** 
     * Méthode permettant de faire persister une instance de Language sur la base de données
     * @param Language le Language à faire persister
     * @return void Aucun retour, mais une fois persisté, le Language aura un id assigné
     */
    
     public function persist(Language $language){

        $statement = $this->connection->prepare('INSERT INTO language (name) VALUES (:name)');

        $statement->bindValue('name', $language->getName(), PDO::PARAM_STR);

        $statement->execute();

        $language->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de User avec le bon id
     * @param Language $language le nouveau user que l'on veut
     * @return void
     */
    public function update(Language $language):void {

        $statement = $this->connection->prepare('UPDATE language SET name = :name WHERE id = :id');

        $statement->bindValue('name', $language->getName(), PDO::PARAM_STR);
        
        $statement->bindValue('id', $language->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un language de la base de donnée à partir de son id
     * @param int $id, l'id du language à supprimer
     * @return void
     */
    public function delete(Language $language)
    { 
        $statement = $this->connection->prepare("DELETE FROM language  WHERE id =:id");
        $statement->bindValue("id", $language->getId(), PDO::PARAM_INT);
        $statement->execute();
        
    }

    /**
     * Permet de trouver tous les teachers en fonction de la langue enseignée
     * @param Language $language la langue dont on veut trouver les profs
     * @return array renvoit un tableau de profs
         */    
        public function findTeacherByLanguage(int $id):array
        {
            $array = [];
            $statement = $this->connection->prepare('SELECT teacher.*
            FROM teacher_language
            LEFT JOIN teacher 
            ON teacher.id = teacher_language.id_teacher
            WHERE teacher_language.id_language = :id');
    
            $statement->bindValue(':id', $id, PDO::PARAM_INT);
            $statement->execute();
            
            $results = $statement->fetchAll();
            foreach($results as $line){
                $teacher = new Teacher($line["name"], $line['lastName'], $line['id_user'], $line['id']);
                $array[] = $teacher;
            }
            return $array;
        }

}