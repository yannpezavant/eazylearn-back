<?php

namespace App\Repository;
use App\Entity\Message;
use App\Entity\Teacher;
use App\Entity\Student;
use DateTime;
use PDO;

class MessageRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Message
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Message l'instance de profil
     */

     public function sqlToMessage(array $line):Message{
        return new Message($line["content"], $line['author'], $line["id_student"], $line["id_teacher"], new DateTime($line["date"]), $line['id']) ;
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Message
     * @return Message[]
     */

     public function findAll():array
     {
         $array = [];
 
         $statement = $this->connection->prepare('SELECT * FROM message');
         
         $statement->execute();
         
         $results = $statement->fetchAll();
         
         foreach($results as $item){
             $array[] = $this->sqlToMessage($item);
         }
         return $array;
     }

/**
     * Méthode qui effectue une requête SQL vers la base de donnée pour trouve un Message spécifique à partir de son id et qui le renvoit comme instance de la classe Message.
     * @param int $id L'id du Message à récupérer
     * @return Message le Message récupéré, instance de la classe Message
     */
    public function findById(int $id):?Message{

        $statement = $this->connection->prepare('SELECT * FROM message WHERE id = :id');

        $statement->bindValue(':id', $id, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetch();

        if($results) {
            return $this->sqlToMessage($results);
        }
        return null;
    }

    /** 
     * Méthode permettant de faire persister une instance de Message sur la base de données
     * @param Message le Message à faire persister
     * @return void Aucun retour, mais une fois persisté, le Message aura un id assigné
     */
    
     public function persist(Message $message){

        $statement = $this->connection->prepare('INSERT INTO message (content, author, id_teacher, id_student,  date) VALUES (:content, :author, :id_teacher, :id_student, :date)');

        $statement->bindValue('content', $message->getContent(), PDO::PARAM_STR);

        $statement->bindValue('author', $message->getAuthor(), PDO::PARAM_STR);

        $statement->bindValue('id_teacher', $message->getIdTeacher(), PDO::PARAM_STR);
        
        $statement->bindValue('id_student', $message->getIdStudent(), PDO::PARAM_STR);

        
        $statement->bindValue('date', $message->getDate()->format('Y-m-d'));

        $statement->execute();

        $message->setId($this->connection->lastInsertId());
    }

    /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de User avec le bon id
     * @param Message $message le nouveau user que l'on veut
     * @return void
     */
    public function update(Message $message):void {

        $statement = $this->connection->prepare('UPDATE message SET content = :content, author = :author, date = :date WHERE id = :id');

        $statement->bindValue('content', $message->getContent(), PDO::PARAM_STR);

        $statement->bindValue('author', $message->getAuthor(), PDO::PARAM_STR);
        
        $statement->bindValue('date', $message->getDate()->format('Y-m-d'));

        $statement->bindValue('id', $message->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un message de la base de donnée à partir de son id
     * @param int $id, l'id du message à supprimer
     * @return void
     */
    public function delete(Message $message)
    { 
        $statement = $this->connection->prepare("DELETE FROM message  WHERE id =:id");
        $statement->bindValue("id", $message->getId(), PDO::PARAM_INT);
        $statement->execute();
        
    }


    /**
     * Methode qui permet de récuperer les message en fonction de l'idStudent et idTeacher
     * @param int $studentId, id du student a recuperer
     * @param int $teacherId, id du teacher a recuperer
     */
    public function getMessagesByStudentAndTeacher(int $studentId, int $teacherId): array
{

    $array = [];
    $statement = $this->connection->prepare(
        'SELECT message.content 
        FROM message 
        WHERE message.id_student = :id_student AND message.id_teacher = :id_teacher'
    );

    $statement->bindValue(':id_student', $studentId, PDO::PARAM_INT);
    $statement->bindValue(':id_teacher', $teacherId, PDO::PARAM_INT);
    
    $statement->execute();
    
    $results = $statement->fetchAll(PDO::FETCH_COLUMN);

    foreach ($results as $row) {
        $message = new Message(
            $row['content'],
            $row['author'],
            $row['id_student'],
            $row['id_teacher'],
            new DateTime($row['date']),
            $row['message_id']
        );
        $array[] = $message;
    }

    return $array;
}
    
public function getMessagesByStudentAndTeacherByIdUser(int $studentId, int $teacherId): array
    {
        $array = [];
        $statement = $this->connection->prepare(
            'SELECT message.id AS message_id, message.content, message.author, message.date,
                    teacher.id AS teacher_id, teacher.name AS teacher_name, teacher.lastName AS teacher_lastName,
                    student.id AS student_id, student.name AS student_name, student.lastName AS student_lastName
             FROM message
             LEFT JOIN teacher ON message.id_teacher = teacher.id
             LEFT JOIN student ON message.id_student = student.id
             LEFT JOIN user AS teacher_user ON teacher.id_user = teacher_user.id
             LEFT JOIN user AS student_user ON student.id_user = student_user.id
             WHERE (teacher_user.id = :teacher_id AND student_user.id = :student_id)
             ORDER BY message.date'
        );

        $statement->bindValue(':student_id', $studentId, PDO::PARAM_INT);
        $statement->bindValue(':teacher_id', $teacherId, PDO::PARAM_INT);

        $statement->execute();

        $results = $statement->fetchAll();

        foreach ($results as $row) {
            $message = new Message(
                $row['content'],
                $row['author'],
                $row['student_id'],
                $row['teacher_id'],
                new DateTime($row['date']),
                $row['message_id']
            );
            $array[] = $message;
        }

        return $array;
    }
}

