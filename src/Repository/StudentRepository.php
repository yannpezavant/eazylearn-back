<?php

namespace App\Repository;
use App\Entity\Student;
use PDO;

class StudentRepository {

    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Student
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Student l'instance de profil
     */
    public function sqlToStudent(array $line):Student {
        return new Student($line["name"], $line["lastName"], $line['id_user'], $line['id']);
    }

    /**
     * Renvoit tous les profs de la table student
     * @return array renvoit un tableau de students de la classe Student
     */
    public function findAll(): array
    {
        $teachers = [];
        $statement = $this->connection->prepare('SELECT * FROM student');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $teachers[] = $this->sqlToStudent($item);
        }
        return $teachers;
    }

    /**
     * Permet de trouver un student grâce à son id
     * @param int $id l'id du student que l'on veut trouver
     * @return Student|null Renvoit un student
     */
    public function findById(int $id):?Student {
        $statement = $this->connection->prepare('SELECT * FROM student WHERE id=:id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if($result) {
            return $this->sqlToStudent($result);
        }
        return null;
    
    }

    /**
     * Permet de faire persister un nouveau student dans la base de données
     * @param Student $student le nouveau student que l'on veut ajouter à la base de données
     * @return void
     */
    public function persist(Student $student) {
        $statement = $this->connection->prepare('INSERT INTO student (id,name,lastName, id_user) VALUES (:id,:name, :lastName, :id_user)');

        $statement->bindValue('name', $student->getName());
        $statement->bindValue('lastName', $student->getlastName());
        $statement->bindValue('id_user', $student->getIdUser());
        $statement->bindValue('id', $student->getId());
        $statement->execute();
    }

    /**
     * Permet de modifier un user dans la base de donnée en lui fournissant une nouvelle instance de student avec le bon id
     * @param Student $student le nouveau student que l'on veut
     * @return void
     */
    public function update(Student $student):void {

        $statement = $this->connection->prepare('UPDATE student SET name = :name, lastName = :lastName WHERE id = :id');

        $statement->bindValue('name', $student->getName(), PDO::PARAM_STR);

        $statement->bindValue('lastName', $student->getlastName(), PDO::PARAM_STR);

        $statement->bindValue('id', $student->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

    /**
     * Méthode qui permet de supprimer un student de la base de donnée à partir de son id
     * @param int $id, l'id du student à supprimer
     * @return void
     */
    public function delete(Student $student)
    { 
        $statement = $this->connection->prepare("DELETE FROM student WHERE id =:id");
        $statement->bindValue("id", $student->getId(), PDO::PARAM_INT);
        $statement->execute();
    }


}