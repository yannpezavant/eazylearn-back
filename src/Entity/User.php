<?php
namespace App\Entity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class User implements UserInterface, PasswordAuthenticatedUserInterface{
    private ?int $id;
	#[Assert\NotBlank]
    private string $role;
    private ?string $image;
	#[Assert\NotBlank]
    private string $mail;
	#[Assert\NotBlank]
    private string $password;

    public function __construct(string $role, ?string $image, string $mail, string $password,  ?int $id = null){
		$this->role = $role;
    	$this->image = $image;
    	$this->mail = $mail;
    	$this->password = $password;
		$this->id = $id;
	}
    
	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getRole(): string {
		return $this->role;
	}

	
	/**
	 * @param  $role 
	 * @return self
	 */
	public function setRole(string $role): self {
		$this->role = $role;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param  $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param  $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getPassword(): string {
		return $this->password;
	}
	
	/**
	 * @param  $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}

	public function getRoles(): array {
        return [$this->role];
	}
	/**
     * Méthode qui sert à remettre à zéro les données sensibles dans l'entité pour ne pas les persistées
     * (par exemple si on avait un champ pour le mot de passe en clair différent du champ password)
     */
	public function eraseCredentials() {
	}
	/**
     * Méthode indiquant à Symfony Security l'identifiant du user, donc le mail.
     */
	public function getUserIdentifier(): string {
        return $this->mail;
	}
}
