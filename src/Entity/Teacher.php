<?php
namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Teacher{
    private ?int $id;
	#[Assert\NotBlank]
    private ?string $name;
    #[Assert\NotBlank]
    private ?string $lastName;
	#[Assert\NotBlank]
    private ?int $idUser;
    
	public function __construct(?string $name, ?string $lastName,?int $idUser, ?int $id = null ){
		$this->id = $id;
    	$this->name = $name;
    	$this->lastName = $lastName;
    	$this->idUser = $idUser;
	}

    

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getLastName(): ?string {
		return $this->lastName;
	}
	
	/**
	 * @param  $lastName 
	 * @return self
	 */
	public function setLastName(?string $lastName): self {
		$this->lastName = $lastName;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdUser(): ?string {
		return $this->idUser;
	}
	
	/**
	 * @param  $idUser 
	 * @return self
	 */
	public function setIdUser(?string $idUser): self {
		$this->idUser = $idUser;
		return $this;
	}
}