<?php
namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class Message {
    private ?int $id;
	#[Assert\NotBlank]
    private ?string $content;
    private ?string $author;
    private ?DateTime $date;
    private ?int $idTeacher;
    private ?int $idStudent;

    public function __construct(?string $content, ?string $author, ?int $idTeacher, ?int $idStudent, ?DateTime $date = new DateTime(),
	?int $id = null){
		$this->id = $id;
		$this->content = $content;
    	$this->author = $author;
    	$this->date = $date;
    	$this->idTeacher = $idTeacher;
    	$this->idStudent = $idStudent;
	}
	
	

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getContent(): ?string {
		return $this->content;
	}
	
	/**
	 * @param  $content 
	 * @return self
	 */
	public function setContent(?string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param  $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdTeacher(): ?int {
		return $this->idTeacher;
	}
	
	/**
	 * @param  $idTeacher 
	 * @return self
	 */
	public function setIdTeacher(?int $idTeacher): self {
		$this->idTeacher = $idTeacher;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdStudent(): ?int {
		return $this->idStudent;
	}
	
	/**
	 * @param  $idStudent 
	 * @return self
	 */
	public function setIdStudent(?int $idStudent): self {
		$this->idStudent = $idStudent;
		return $this;
	}
}