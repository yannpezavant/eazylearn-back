<?php
namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Ranking {
    private ?int $id;
	#[Assert\NotBlank]
    private ?int $note;
    private ?int $idTeacher;
    private ?int $idStudent;

    public function __construct(?string $note, ?int $idTeacher, ?int $idStudent, ?int $id = null){
		$this->id = $id;
    	$this->note = $note;
    	$this->idTeacher = $idTeacher;
    	$this->idStudent = $idStudent;
	}
    


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getNote(): ?int {
		return $this->note;
	}
	
	/**
	 * @param  $note 
	 * @return self
	 */
	public function setNote(?int $note): self {
		$this->note = $note;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdTeacher(): ?int {
		return $this->idTeacher;
	}
	
	/**
	 * @param  $idTeacher 
	 * @return self
	 */
	public function setIdTeacher(?int $idTeacher): self {
		$this->idTeacher = $idTeacher;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdStudent(): ?int {
		return $this->idStudent;
	}
	
	/**
	 * @param  $idStudent 
	 * @return self
	 */
	public function setIdStudent(?int $idStudent): self {
		$this->idStudent = $idStudent;
		return $this;
	}
}