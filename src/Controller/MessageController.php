<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\MessageRepository;
use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/message')]
class MessageController extends AbstractController
{

    public function __construct(private MessageRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $message = $this->repo->findById($id);
        if (!$message) {
            throw new NotFoundHttpException();
        }

        return $this->json($message);

    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {

        try {
            /**
             * @var User
             */
            $user = $this->getUser();

            $message = $serializer->deserialize($request->getContent(), Message::class, 'json');
            $message->setDate(new \DateTime());
            //set id student à partir du getUser
            if ($user->getRole() == 'ROLE_STUDENT') {
                $message->setIdStudent($user->getId());

            } else {
                $message->setIdTeacher($user->getId());

            }

            $this->repo->persist($message);
            return $this->json($message, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $message = $this->repo->findById($id);
        if (!$message) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Message::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $message = $this->repo->findById($id);
        if (!$message) {
            throw new NotFoundHttpException();
        }
        $this->repo->delete($message);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route('/student/{studentId}/teacher/{teacherId}', methods: 'GET')]
    public function messagesByStudentAndTeacher(int $studentId, int $teacherId): Response
    {
        $messages = $this->repo->getMessagesByStudentAndTeacherByIdUser($studentId, $teacherId);

        return $this->json($messages);
    }


}