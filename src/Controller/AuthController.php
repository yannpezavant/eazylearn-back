<?php

namespace App\Controller;

use App\Entity\Student;
use App\Entity\Teacher;
use App\Entity\User;
use App\Repository\MessageRepository;
use App\Repository\StudentRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    public function __construct(private UserRepository $repo, private TeacherRepository $repoTeacher, private StudentRepository $repoStudent ) {
    }

    #[Route('/api/user', methods: 'POST')]
    public function register(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher, JWTTokenManagerInterface $jwtManager): Response
    {
        $user = $serializer->deserialize($request->getContent(), User::class, 'json');
        $data = $request->toArray(); //recuperer data sous forme de tableau associatif
    
        //On vérifie s'il n'y a pas déjà un user avec cet email en base de données
        if($this->repo->findByEmail($user->getMail())) {
            return $this->json(['error' => 'User already exists'], Response::HTTP_BAD_REQUEST);
        }
        //On hash son mot de passe (symfony rajoute automatiquement sel et probablement poivre)
        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);
        //On lui assigne un rôle par défaut
        $this->repo->persist($user);
        if ($user->getRole() === 'ROLE_TEACHER') {

            $teacher = new Teacher($data["lastname"], $data["name"], $user->getId(), $user->getId());
            // Initialise les propriétés spécifiques à un enseignant

            $this->repoTeacher->persist($teacher);

        } elseif ($user->getRole() === 'ROLE_STUDENT') {
            $student = new Student($data["lastname"], $data["name"], $user->getId(), $user->getId());
            $this->repoStudent->persist($student);
        }
            return $this->json($user);
        
    }

    #[Route('/api/account', methods: 'GET')]
    public function fetchUserConnected(): Response
    {
        return $this->json($this->getUser());
        
    }


    #[Route('/api/account/teacher/', methods: ['GET'])]
    public function findByMessage(TeacherRepository $teacherRepository): Response
    {
        /**
         * @var User
         */
        $user = $this->getUser();
        $teachers = $teacherRepository->findByMessage($user->getId());
        
        return $this->json($teachers);
    }


    #[Route('/api/account/teacher/{id}', methods: ['GET'])]
    public function getMessageByTeacher(int $id, TeacherRepository $teacherRepository): Response
    {
        $messages = $teacherRepository->findById($id);

        return $this->json($messages);
    }


    // #[Route('/api/account/teacher/{id}', methods: ['GET'])]
    // public function teacherByLanguage(int $id, TeacherRepository $teacherRepository): Response
    // {
    //     $messages = $teacherRepository->findById($id);

    //     return $this->json($messages);
    // }




}

