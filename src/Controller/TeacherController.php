<?php

namespace App\Controller;
use App\Entity\User;
use App\Repository\TeacherRepository;
use App\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/teacher')]
class TeacherController extends AbstractController
{

    public function __construct(private TeacherRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }
    
    
    #[Route('/students', methods: 'GET')]
    public function studentsByTeacher(): Response
    {

        /**
         * @var User
         */
        $user = $this->getUser();

        $students = $this->repo->findStudentsByTeacher($user->getId());
    
        if (!$students) {
            throw new NotFoundHttpException();
        }
    
        return $this->json($students);
    }

    // #[Route('/{id}', methods: 'GET')]
    
    // public function one(teacher $teacher)
    // {
    //     return $this->json($teacher);
    // }
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $teacher = $this->repo->findById($id);
        if(!$teacher){
            throw new NotFoundHttpException();
        }
        
        return $this->json($teacher);
 
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $user = $serializer->deserialize($request->getContent(), Teacher::class, 'json');
            $this->repo->persist($user);

            return $this->json($user, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
        
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $teacher = $this->repo->findById($id);
        if(!$teacher){
            throw new NotFoundHttpException();
        }
        try{
            $toUpdate = $serializer->deserialize($request->getContent(), Teacher::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
    
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
 
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $teacher = $this->repo->findById($id);
        if(!$teacher){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($teacher);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/message', methods: 'GET')]
    public function messageByTeacher(int $id) {
        $languageTeacher = $this->repo->findMessageByTeacher($id);

        if(!$languageTeacher){
            throw new NotFoundHttpException();
        }
        return $this->json($languageTeacher);
    }

    #[Route('/{id}/language', methods: 'GET')]
    public function languageByTeacher(int $id) {
        $teacherLanguage = $this->repo->findLanguageByTeacher($id);

        if(!$teacherLanguage){
            throw new NotFoundHttpException();
        }
        return $this->json($teacherLanguage);
    }


    #[Route('/{id}/image', methods: 'GET')]
    public function getImageForTeacher(int $id, TeacherRepository $teacherRepository)
    {
        $image = $teacherRepository->findImageByTeacherId($id);

        if (!$image) {
            throw $this->createNotFoundException('Image not found');
        }

        // return new JsonResponse(['image' => $image]);
        return $this->json($image);
    }

    #[Route('/teachers/details', methods: ['GET'])]
        public function teacherDetails()
        {
            $teacherDetails = $this->repo->findAllTeachersWithDetails();

            if (!$teacherDetails) {
                throw new NotFoundHttpException();
            }

            return $this->json($teacherDetails);
        }


    #[Route('/{id}/details', methods: 'GET')]
         public function teacherDetailsById(int $id)
        {
            $teacherDetails = $this->repo->fetchTeacherDetailsById($id);

            if (!$teacherDetails) {
                throw new NotFoundHttpException();
            }

            return $this->json($teacherDetails);
        }


}