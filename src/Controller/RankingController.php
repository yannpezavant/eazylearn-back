<?php

namespace App\Controller;
use App\Repository\RankingRepository;
use App\Entity\Ranking;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/ranking')]
class RankingController extends AbstractController
{

    public function __construct(private RankingRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $ranking = $this->repo->findById($id);
        if(!$ranking){
            throw new NotFoundHttpException();
        }
        
        return $this->json($ranking);
 
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $ranking = $serializer->deserialize($request->getContent(), Ranking::class, 'json');
            $this->repo->persist($ranking);

            return $this->json($ranking, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $ranking = $this->repo->findById($id);
        if(!$ranking){
            throw new NotFoundHttpException();
        }
        try{
            $toUpdate = $serializer->deserialize($request->getContent(), Ranking::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
    
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
 
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $ranking = $this->repo->findById($id);
        if(!$ranking){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($ranking);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

}