<?php

namespace App\Controller;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/user')]
class UserController extends AbstractController
{

    public function __construct(private UserRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }


    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();
        }
        
        return $this->json($user);
 
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json');
            $this->repo->persist($user);

            return $this->json($user, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
        
    }


    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();
        }
        try{
            $toUpdate = $serializer->deserialize($request->getContent(), User::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
    
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
 
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $user = $this->repo->findById($id);
        if(!$user){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($user);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    


}