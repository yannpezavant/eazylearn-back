<?php

namespace App\Controller;
use App\Entity\Teacher;
use App\Repository\LanguageRepository;
use App\Entity\Language;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/language')]
class LanguageController extends AbstractController
{

    public function __construct(private LanguageRepository $repo)
    {
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id) {
        $language = $this->repo->findById($id);
        if(!$language){
            throw new NotFoundHttpException();
        }
        
        return $this->json($language);
 
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer)
    {
        try {
            $language = $serializer->deserialize($request->getContent(), Language::class, 'json');
            $this->repo->persist($language);

            return $this->json($language, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
        
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $language = $this->repo->findById($id);
        if(!$language){
            throw new NotFoundHttpException();
        }
        try{
            $toUpdate = $serializer->deserialize($request->getContent(), Language::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
    
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
 
    }

    #[Route('/{id}', methods: 'GET')]
    public function remove(int $id) {   
        $language = $this->repo->findById($id);
        if(!$language){
            throw new NotFoundHttpException();
        }
        $this->repo->delete($language);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}/teacher', methods: 'GET')]
    public function teacherByLanguage(int $id) {
        $languageTeacher = $this->repo->findTeacherByLanguage($id);

        if(!$languageTeacher){
            throw new NotFoundHttpException();
        }
        return $this->json($languageTeacher);
    }

}