-- Active: 1673947702029@@127.0.0.1@3306@eazylearn
DROP TABLE IF EXISTS teacher_language;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS ranking;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS teacher;
DROP TABLE IF EXISTS language;
DROP TABLE IF EXISTS user;


CREATE TABLE user(  
    id int PRIMARY KEY AUTO_INCREMENT,
    role VARCHAR(100),
    image VARCHAR(10000),
    mail VARCHAR(255),
    password VARCHAR(255)
);
INSERT INTO user (role, image, mail, password) VALUES 
("ROLE_TEACHER", "https://plus.unsplash.com/premium_photo-1671656333511-1b43049ea1a0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8ZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60", "marie.dupont@gmail.com",
"$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_TEACHER", "https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8ZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60", "thomas.latour@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_TEACHER", "https://images.unsplash.com/photo-1499996860823-5214fcc65f8f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8ZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60", "etienne.lepage@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_TEACHER", "https://images.unsplash.com/photo-1557296387-5358ad7997bb?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8ZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60", "aurore.durant@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_TEACHER", "https://images.unsplash.com/photo-1542909168-82c3e7fdca5c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8ZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60", "francois.dupont@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_STUDENT", "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTF8fGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60", "mathias.dandois@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_STUDENT", "https://plus.unsplash.com/premium_photo-1664203068007-52240d0ca48f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTN8fGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60", "nathalie.lapage@gmail.com","$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_STUDENT", "https://images.unsplash.com/photo-1545996124-0501ebae84d0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60", "henry.couzon@gmail.com",
"$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_STUDENT", "https://images.unsplash.com/photo-1545996124-0501ebae84d0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60", "damien.blanc@gmail.com",
"$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq"),
("ROLE_STUDENT", "https://images.unsplash.com/photo-1593529467220-9d721ceb9a78?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60", "salma.bensaid@gmail.com",
"$2y$13$wFgmMWedqgRnYY82uk25iu0v2KqKfxQVojF3KlV7ryzIMuMeR4Khq");


CREATE TABLE language(  
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (255)
);
INSERT INTO language (name) VALUES 
('anglais'),
('espagnol'),
('italien'),
('allemand');


CREATE TABLE teacher(  
    id int PRIMARY KEY,
    name VARCHAR(255),
    lastName VARCHAR(255),
    id_user int,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE
);
INSERT INTO teacher (id, name, lastName, id_user) VALUES 
(1,"Marie","Dupont",1),
(2,"Thomas","Latour",2),
(3,"Etienne","Lepage",3),
(4,"Aurore","Durant",4),
(5,"François","Dupont",5);

CREATE TABLE student(  
    id int PRIMARY KEY,
    name VARCHAR(255),
    lastName VARCHAR(255),
    id_user int,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE
);

INSERT INTO student (id, name, lastName, id_user) VALUES 
(6,"Mathias","Dandois",6),
(7,"Nathalie","Lapage",7),
(8,"Henry","Couzon",8),
(9,"Damien","Blanc",9),
(10,"Salma","Bensaid",10);

CREATE TABLE ranking(  
    id int PRIMARY KEY AUTO_INCREMENT,
    note int,
    id_teacher int,
    id_student int,
    Foreign Key (id_teacher) REFERENCES teacher(id) ,
    Foreign Key (id_student) REFERENCES student(id) 
);
INSERT INTO ranking (note, id_teacher, id_student) VALUES 
(1,1,6),(2,2,7),(3,3,8),(4,4,9),(5,4,10);

CREATE TABLE message(  
    id int PRIMARY KEY AUTO_INCREMENT,
    content VARCHAR(255),
    author VARCHAR(255),
    date DATE,
    id_teacher int,
    id_student int,
    Foreign Key (id_teacher) REFERENCES teacher(id) ON DELETE CASCADE,
    Foreign Key (id_student) REFERENCES student(id) ON DELETE CASCADE
);
INSERT INTO message (content, author, date, id_teacher, id_student) VALUES 
("Bonjour, je souhaite apprendre l'anglais","Mathias Dandois",'2023-08-05',1,6),
("Bonjour, j'ai besoin d'aide en espagnol svp.","Nathalie Lapage",'2023-08-15',2,7),
("Bjr, j'ai besoin d'un renforcement en italien, merci.","Henry Couzon",'2023-08-10',3,8),
("Slt, je voudrais des cours en italien, vous êtes dispo?","Damien Blanc",'2023-08-01',4,9),
("Bonjour monsieur, pouvez-vous m'aider en allemand svp, merci.","Salma Bensaid",'2023-08-20',5,10);



CREATE TABLE teacher_language (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_teacher INT,
    id_language INT,
    Foreign Key (id_teacher) REFERENCES teacher(id),
    Foreign Key (id_language) REFERENCES language(id)
);
INSERT INTO teacher_language (id_teacher, id_language) VALUES 
(1,1),(2,2),(3,3),(4,4),(5,1);


SELECT DISTINCT t.id AS teacher_id, t.name AS teacher_name, t.lastName AS teacher_lastName, 
m.id AS message_id, m.content, m.author, m.date
FROM teacher t
JOIN message m ON t.id = m.id_teacher;

/*selectionne l eteacher en fonction de l'id du message du student*/
SELECT DISTINCT teacher.* 
FROM teacher 
INNER JOIN message 
ON teacher.id = message.id_teacher
WHERE message.id_student = 6



/*messages selon id student*/
SELECT DISTINCT teacher.id AS teacher_id, teacher.name AS teacher_name, teacher.lastName AS teacher_lastName, 
                message.id AS message_id, message.content, message.author, message.date
FROM teacher 
INNER JOIN message 
ON teacher.id = message.id_teacher
WHERE message.id_student = id_student;


/*message selon id teacher*/
SELECT DISTINCT message.*
FROM teacher 
LEFT JOIN message 
ON teacher.id = message.id_teacher
WHERE message.id_teacher = :id
LIMIT 1;


/*trouver les profs en fonction de la langue enseignée*/
SELECT teacher.*
FROM teacher_language
LEFT JOIN teacher 
ON teacher.id = teacher_language.id_teacher
WHERE teacher_language.id_language = :id;


/*find by message*/
SELECT DISTINCT teacher.* 
FROM teacher 
INNER JOIN message ON teacher.id = message.id_teacher
WHERE message.id_student = :id

/*findLanguageByTeacher*/
SELECT language.*
FROM teacher
JOIN teacher_language ON teacher.id = teacher_language.id_teacher
JOIN language ON teacher_language.id_language = language.id
WHERE teacher.id = 1;


/*image pour role_teacher et id_user*/
SELECT u.image
FROM user u
JOIN teacher t 
ON u.id = t.id_user
WHERE t.id = 1;

/*recupere les infos et image des teachers selon id_teacher*/
SELECT teacher.name AS teacher_name, teacher.lastName AS teacher_lastName, user.image
FROM teacher
JOIN user 
ON teacher.id_user = user.id
WHERE teacher.id = 1;

/*recupere infos, img et language des teachers selon idTeacher*/
SELECT teacher.name AS teacher_name, teacher.lastName AS teacher_lastName, user.image, language.name AS language
FROM teacher
JOIN user 
ON teacher.id_user = user.id
JOIN teacher_language 
ON teacher.id = teacher_language.id_teacher
JOIN language 
ON teacher_language.id_language = language.id
WHERE teacher.id = :id;


/*name lastName image et language de tous les teachers*/
SELECT teacher.name AS name, teacher.lastName AS lastName, user.image, language.name AS language
FROM teacher
JOIN user 
ON teacher.id_user = user.id
JOIN teacher_language 
ON teacher.id = teacher_language.id_teacher
JOIN language 
ON teacher_language.id_language = language.id;


SELECT DISTINCT teacher.* 
FROM teacher 
INNER JOIN message 
ON teacher.id = message.id_teacher
WHERE message.id_student = 7;

/*fetch Msg by Student id*/
SELECT message.id, message.content, message.author, message.date
FROM message
JOIN teacher 
ON message.id_teacher = teacher.id
WHERE message.id_student = 6;


SELECT DISTINCT message.id, message.content, message.author, message.date,
       teacher.id AS teacher_id, teacher.name AS teacher_name, teacher.lastName AS teacher_lastName
FROM message
LEFT JOIN teacher 
ON message.id_teacher = teacher.id
WHERE message.id_student = :student_id 
AND teacher.id = :teacher_id;


SELECT message.content 
        FROM message 
        WHERE message.id_student = :student_id 
        AND message.id_teacher = :teacher_id;

/*identifiants des personnes ayant envoyé un message a un teacher*/
SELECT DISTINCT student.name, student.lastName
FROM student
JOIN message ON student.id = message.id_student
WHERE message.id_teacher = :id;




/*message entre teacher et student*/
SELECT message.id AS message_id, message.content, message.author, message.date,
       teacher.id AS teacher_id, teacher.name AS teacher_name, teacher.lastName AS teacher_lastName,
       student.id AS student_id, student.name AS student_name, student.lastName AS student_lastName
FROM message
LEFT JOIN teacher ON message.id_teacher = teacher.id
LEFT JOIN student ON message.id_student = student.id
WHERE (teacher.id = 1 AND student.id = 6)
ORDER BY message.date;


/*message entre teacher et student selon idUser de chacun*/
SELECT message.id AS message_id, message.content, message.author, message.date,
       teacher.id AS teacher_id, teacher.name AS teacher_name, teacher.lastName AS teacher_lastName,
       student.id AS student_id, student.name AS student_name, student.lastName AS student_lastName
FROM message
LEFT JOIN teacher ON message.id_teacher = teacher.id
LEFT JOIN student ON message.id_student = student.id
LEFT JOIN user AS teacher_user ON teacher.id_user = teacher_user.id
LEFT JOIN user AS student_user ON student.id_user = student_user.id
WHERE (teacher_user.id = :id AND student_user.id = :id)
ORDER BY message.date;
