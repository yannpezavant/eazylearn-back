<?php

namespace App\Tests;

use App\Repository\Database;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MessageTest extends WebTestCase
{

    /**
     * La méthode setUp sera déclenchée avant l'exécution de chacun des tests de la classe actuelle.
     * Ici on lui dit de remettre à zéro la bdd en se basant sur le contenu du database.sql
     */
    public function setUp():void {
        Database::connect()->query(file_get_contents(__DIR__.'/../database.sql'));
    }
    public function testFindById() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/message');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(5, $body);
        $item = $body[0];
        $this->assertIsInt($item['id']);
        $this->assertIsString($item['content']);
        $this->assertIsString($item['author']);
        new \DateTime($item['date']);
        $this->assertIsInt($item['idStudent']);
        $this->assertIsInt($item['idTeacher']); 
    }

    public function testGetById() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/message/1');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);
        $this->assertIsString($body['content']);
        $this->assertIsString($body['author']);
        new \DateTime($body['date']);
        $this->assertIsInt($body['idStudent']);
        $this->assertIsInt($body['idTeacher']); 
    }

    public function testAddMessage() {
        
        $client = static::createClient();

        $json = json_encode([
            'content' => 'test content'
        ]);
        
        $client->request('POST', '/api/message', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertIsInt($body['id']);

    }

    public function testUpdatePicture() {
        
        $client = static::createClient();

        $json = json_encode([
            'content' => 'test content'
        ]);
        
        $client->request('PUT', '/api/message/1', content: $json);

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);
        
        $this->assertEquals($body['id'], 1);
        $this->assertEquals($body['content'], 'test content');

    }

    public function testDelete() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/message/1');

        $this->assertResponseIsSuccessful();
        
    }

    public function testDeleteNotFound() {
        
        $client = static::createClient();
        
        $client->request('DELETE', '/api/picture/100');

        $this->assertResponseStatusCodeSame(404);
    }
    
}